# Twitch-Tools-in-Python
Free for all

Version 0.0 Features:
- print number of followers

Version 0.1 Features:
- print number of followers
- print last follower 

Version 0.2 Beta Features:
- print number of followers
- print last follower 
- show ratio all followers to followers goal

Version 0.3 Beta Features: 
- print number of followers
- print last follower 
- show ratio all followers to followers goal
- write last follower to txt file, easy to show up by OBS

Version 0.4 Beta Features:
- print number of followers
- print last follower 
- show ratio all followers to followers goal
- write last follower to txt file, easy to show up by OBS
- write number of all followers to text file, easy to show up by OBS

Version 0.5 Alfa (Not all features posted on github) Features:
- print number of followers //(posted)
- print last follower //(posted)
- show ratio all followers to followers goal //(posted)
- write last follower to txt file, easy to show up by OBS //(posted)
- write number of all followers to text file, easy to show up by OBS //(posted)
- testing sub goals and followers goals (Beta) 
- testing OAuth for subs (Alfa)
- showing image when someone start following profile (Alfa, posted by commented because of working issues)
- playing music when someone start following profile (Alfa)
